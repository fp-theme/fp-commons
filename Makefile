
.PHONY: default preview sass

default: preview

sass:
	rm public/fp.css.map
	sass --no-source-map sass/fp.scss public/fp.css

sass-debug:
	sass sass/fp.scss public/fp.css

preview: sass-debug
	xdg-open public/index.html
