# Commons #

Common colors, swatches, images, and everything needed everywhere else in @fp-theme

[Preview](https://fp-theme.gitlab.io/fp-commons/)

## Design inspirations ##

Over the canopy, just above the horizon, a lone mountain brushes the sun.

[![https://source.unsplash.com/aqZ3UAjs_M4](https://source.unsplash.com/aqZ3UAjs_M4)](https://unsplash.com/photos/aqZ3UAjs_M4)
Palette:
- Peru `#cf7b3d`
- Dark Olive Green `#574036`
- Silver `#cbc7ba`
- Light Slate Grey `#9ba7ab`
- Slate Grey `#808282`

Under the canopy, a river flows quietly.

[![https://source.unsplash.com/8sOZJ8JF0S8](https://source.unsplash.com/8sOZJ8JF0S8)](https://unsplash.com/photos/8sOZJ8JF0S8)
Palette:
- Brick Red `#d53a44`
- Burly Wood `#e39d9b`
- Black `#261317`
- Old Rose `#a35256`
- Dark Grey `#b1a0ac`

A few ways away, the sun filters through sturdy tall herbs.

[![https://source.unsplash.com/E0bj4Q53_Dw](https://source.unsplash.com/E0bj4Q53_Dw)](https://unsplash.com/photos/E0bj4Q53_Dw)
Palette:
- Dark Green `#062a1c`
- Dark Slate Grey `#1c433c`
- Tan `#bfd092`
- Dark Olive Green `#587c47`
- Dark Olive Green `#769f4d`

In the city, a tower reaches for the clouds.

[![https://source.unsplash.com/7H77FWkK_x4](https://source.unsplash.com/7H77FWkK_x4)](https://unsplash.com/photos/7H77FWkK_x4)
Palette:
- Teal `#0e6278`
- Brick Red `#d02c2b`
- Tan `#d79889`
- Dim Grey `#915e5d`
- Dim Grey `#5c4656`
